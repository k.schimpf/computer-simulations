use rand::distributions::{Distribution, Uniform};

#[derive(Clone, Debug)]
pub struct Position
{
    pub x: f64,
    pub y: f64,
    pub z: f64
}

impl Position
{
    pub fn from_random(distribution: Uniform<f64>) -> Position
    {
        let x_pos: f64 = distribution.sample(&mut rand::thread_rng());
        let y_pos: f64 = distribution.sample(&mut rand::thread_rng());
        let z_pos: f64 = distribution.sample(&mut rand::thread_rng());

        Position{x: x_pos, y: y_pos, z: z_pos}
    }

    fn wrap_distance(abs: f64, wrap: f64) -> f64
    {
        if abs > wrap/2.0
        {
            return wrap-abs;
        }
        return abs;
    }

    pub fn distance(&self, other: &Position, wrap: f64) -> f64
    {
        let x_distance = (Position::wrap_distance((self.x - other.x).abs(), wrap)).powf(2.0);
        let y_distance = (Position::wrap_distance((self.y - other.y).abs(), wrap)).powf(2.0);
        let z_distance = (Position::wrap_distance((self.z - other.z).abs(), wrap)).powf(2.0);

        (x_distance + y_distance + z_distance).sqrt()
    }
}