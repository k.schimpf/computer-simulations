use crate::phase_space::PhaseSpace;

pub fn myst_step(current_state: PhaseSpace, time_step: f64) -> PhaseSpace
{
    let new_momentum: f64 = current_state.momentum - current_state.position * time_step;
    let new_position: f64 = current_state.position + new_momentum * time_step;

    PhaseSpace {position: new_position, momentum: new_momentum}
}