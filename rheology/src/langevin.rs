use crate::model::Model;
use crate::rouse_chain::{Particle, RouseChain};
use crate::vector::Vector3d;

#[derive(Clone)]
pub struct Langevin
{
    pub rouse_chain: RouseChain,
    friction_coefficient: f64,
    time_step: f64
}

impl Langevin
{
    pub fn new(rouse_chain: RouseChain, friction_coefficient: f64, time_step: f64) -> Langevin
    {
        Langevin {
            rouse_chain,
            friction_coefficient,
            time_step
        }
    }

    fn force_particle(&self, index: usize, particle_position: &Vector3d) -> Vector3d
    {
        let mut potential: Vector3d = Vector3d::get_zero();

        if index > 0
        {
            potential = particle_position.add(
                &self.rouse_chain.polymer[index-1].position.multiply(-1.0)
            )
        }

        if index + 1 < self.rouse_chain.polymer.len()
        {
            potential = potential.add(
                &particle_position.add(
                    &self.rouse_chain.polymer[index+1].position.multiply(-1.0)
                )
            );
        }

        potential.multiply(-3.0 * ((self.rouse_chain.polymer.len() as f64) - 1.0))
    }

    fn new_force_particle(&self, index: usize, particle_position: &Vector3d, new_polymer: &Vec<Particle>) -> Vector3d
    {
        let mut potential: Vector3d = Vector3d::get_zero();

            if index > 0
            {
                potential = particle_position.add(
                    &new_polymer[index-1].position.multiply(-1.0)
                )
            }

            if index + 1 < self.rouse_chain.polymer.len()
            {
                potential = potential.add(
                    &particle_position.add(
                        &self.rouse_chain.polymer[index+1].position.multiply(-1.0)
                    )
                );
            }

            potential.multiply(-3.0/2.0 * ((self.rouse_chain.polymer.len() as f64) - 1.0))
    }

    fn calc_noise(&self) -> Vector3d
    {
        let weight = (2.0 * self.friction_coefficient * self.time_step).sqrt();
        let random = Vector3d::gen_maxwell(1.0);

        random.multiply(weight)
    }

    fn step_particle(&self, index: usize, new_polymer: &Vec<Particle>) -> Particle
    {
        let particle =  &self.rouse_chain.polymer[index];
        let force = self.force_particle(index, &particle.position);

        let noise = self.calc_noise();

        let b = 1.0 + self.friction_coefficient * self.time_step/2.0;
        let a = (1.0 - self.friction_coefficient * self.time_step/2.0)/b;

        let mut position_change = particle.momentum.multiply(self.time_step);
        position_change = position_change.add(&force.multiply(0.5 * self.time_step.powf(2.0)));
        position_change = position_change.add(&noise.multiply(0.5 * self.time_step));

        let mut new_position = particle.position.add(&position_change.multiply(b));
        new_position = new_position.add(&force.multiply(0.5 * self.time_step.powf(2.0)));

        let new_force = self.new_force_particle(index, &new_position, new_polymer);

        let mut momentum_change = (force.multiply(a).add(&new_force)).multiply(0.5 * self.time_step);
        momentum_change = momentum_change.add(&noise.multiply(b));

        let new_momentum = particle.momentum.multiply(a).add(&momentum_change);

        Particle{position: new_position, momentum: new_momentum}
    }

}

impl Model for Langevin
{
    fn step(&mut self)
    {
        let mut next_polymer:Vec<Particle> = vec![];

        for index in 0..self.rouse_chain.polymer.len()
        {
            next_polymer.push(self.step_particle(index, &next_polymer));
        }

        self.rouse_chain.polymer = next_polymer;
    }

    fn get_model(&self) -> &RouseChain
    {
        &self.rouse_chain
    }
}
