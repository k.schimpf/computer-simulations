pub trait Model
{
    fn step(& mut self);
    fn print(&self);
    fn get_number_particles(&self) -> usize;
}