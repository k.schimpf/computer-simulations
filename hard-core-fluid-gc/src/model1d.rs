use std::cell::Cell;
use rand::Rng;
use rand::distributions::{Distribution, Uniform};
use rand_distr::num_traits::ToPrimitive;
use crate::model::Model;

pub struct Particle
{
    pub id: usize,
    pub position: Cell<f64>
}


pub struct Model1D
{
    move_type_distr: Uniform<u8>,
    space_distr: Uniform<f64>,
    accept_distr: Uniform<f64>,

    line_length: f64,
    particle_size: f64,
    activity: f64,

    particle_id_counter: usize,
    particles: Vec<Particle>
}

impl Model1D
{
    fn generate_particles(line_length: f64, particle_size: f64, particle_number: u8) -> Vec<Particle>
    {
        let mut particles: Vec<Particle> = Vec::new();
        let effective_length = line_length - particle_size;

        if effective_length/particle_number.to_f64().unwrap() < particle_size/2.0
        {
            panic!("To many particles for line length!")
        }

        for i in 0..particle_number
        {
            let position: f64 = particle_size/2.0 + i.to_f64().unwrap() * effective_length/particle_number.to_f64().unwrap();
            let particle = Particle {id: i.to_usize().unwrap(), position: Cell::new(position)};

            particles.push(particle);
        }

        particles
    }

    pub fn new(line_length: f64, particle_size: f64, particle_number: u8, activity: f64) -> Model1D
    {
        let move_type_distr: Uniform<u8> = Uniform::new_inclusive(0, 1);
        let space_distr: Uniform<f64> = Uniform::new_inclusive(0.0, line_length);
        let accept_distr: Uniform<f64> = Uniform::new_inclusive(0.0, 1.0);
        let particles: Vec<Particle> = Model1D::generate_particles(line_length, particle_size, particle_number);

        Model1D {
            move_type_distr,
            space_distr,
            accept_distr,
            line_length,
            particle_size,
            particle_id_counter: particles.len() - 1,
            particles,
            activity
        }
    }

    fn check_particle_inside_bounds(&self, particle: &Particle) -> bool
    {
        self.particle_size/2.0 < particle.position.get() && particle.position.get() <= self.line_length - self.particle_size/2.0
    }

    fn check_particle_collides(&self, particle: &Particle) -> bool
    {
        let particle_iterator = self.particles.iter();

        for other_particle in particle_iterator
        {
            if other_particle.id == particle.id
            {
                continue;
            }

            if (other_particle.position.get() - particle.position.get()).abs() < self.particle_size
            {
                return true
            }
        }
        return false
    }

    fn check_particle_validity(&self, particle: &Particle) -> bool
    {
        if !self.check_particle_inside_bounds(particle)
        {
            return false
        }else if self.check_particle_collides(particle)
        {
            return false
        }
        return true
    }

    fn accept_insert(&self, particle: &Particle) -> bool
    {
        if !self.check_particle_validity(particle)
        {
            return false
        }

        let accept_prop:f64 = (self.activity * self.line_length / self.particles.len().to_f64().unwrap()).min(1.0);

        let sample = self.accept_distr.sample(&mut rand::thread_rng());

        return sample <= accept_prop;
    }

    fn accept_delete(&self) -> bool
    {
        let accept_prop:f64 = (self.particles.len().to_f64().unwrap() / (self.activity * self.line_length)).min(1.0);

        let sample = self.accept_distr.sample(&mut rand::thread_rng());

        return sample <= accept_prop;
    }

    fn generate_position(&self) -> f64
    {
        self.space_distr.sample(&mut rand::thread_rng())
    }

    fn choose_particle(&self) -> usize
    {
        let len = self.particles.len();
        rand::thread_rng().gen_range(0..len)
    }

    fn choose_move_type(&self) -> bool
    {
        self.move_type_distr.sample(&mut rand::thread_rng()) == 0
    }

    fn step_insert(& mut self)
    {
        let particle_id = self.particle_id_counter + 1;
        let position = self.generate_position();
        let particle = Particle { id: particle_id, position: Cell::new(position) };
        self.particles.push(particle);

        if !self.accept_insert(&self.particles.last().unwrap())
        {
            self.particles.pop();
        }else {
            self.particle_id_counter = particle_id;
        }

    }

    fn step_delete(& mut self)
    {
        let particle_index = self.choose_particle();
        let particle = self.particles.remove(particle_index);

        if !self.accept_delete()
        {
            self.particles.push(particle)
        }
    }
}

impl Model for Model1D
{
    fn step(& mut self)
    {
        match self.choose_move_type()
        {
            true => {self.step_insert();},
            false => { self.step_delete();}
        }
    }

    fn print(&self)
    {
        let particle_iterator = self.particles.iter();

        for particle in particle_iterator
        {
            print!("{} \t", particle.position.get());
        }
        println!();
    }

    fn get_number_particles(&self) -> usize
    {
        return self.particles.len();
    }
}