use rand::distributions::{Distribution, Uniform};
use rand_distr::num_traits::ToPrimitive;
use crate::lattice::Lattice;

pub struct MetropolisModel
{
    lattice: Lattice,
    move_prob: Uniform<usize>,
    acc_prob: Uniform<f64>,
    interaction_constant: f64
}

impl MetropolisModel
{
    pub fn new(interaction_constant: f64, lattice: Lattice) -> MetropolisModel
    {
        let move_prob: Uniform<usize> = Uniform::new_inclusive(0, lattice.size().pow(2)-1);
        let acc_prob: Uniform<f64> = Uniform::new_inclusive(0.0, 1.0);

        MetropolisModel {lattice, move_prob, acc_prob, interaction_constant}
    }

    fn choose_particle(&self) -> usize
    {
        self.move_prob.sample(&mut rand::thread_rng())
    }

    fn calculate_energy_difference(&self, i: i64, j: i64) -> f64
    {
        let spin = -self.lattice.get_spin(i, j);
        (-2 * spin * self.lattice.sum_neighbours(i, j)).to_f64().unwrap() * self.interaction_constant
    }

    fn accept_move(&self, energy_difference: f64) -> bool
    {
        if energy_difference < 0.0
        {
            return true;
        }

        let acceptance_prob = (-1.0 * energy_difference).exp();
        let sample = self.acc_prob.sample(&mut rand::thread_rng());

        return sample <= acceptance_prob;
    }

    pub fn step(&mut self) -> f64
    {
        let particle_number = self.choose_particle();
        let i = particle_number / self.lattice.size();
        let j = particle_number - i * self.lattice.size();

        let energy_difference = self.calculate_energy_difference(i as i64, j as i64);
        match self.accept_move(energy_difference)
        {
            true => {
                self.lattice.flip_spin(i, j);
                energy_difference
            },
            false => 0.0
        }
    }

    pub fn calc_energy(&self) -> f64
    {
        let mut pairs: i64 = 0;
        for i in 0..self.lattice.size()
        {
            for j in 0..self.lattice.size()
            {
                pairs = pairs + self.lattice.get_spin(i as i64, j as i64) * self.lattice.get_spin((i + 1) as i64, j as i64);
                pairs = pairs + self.lattice.get_spin(i as i64, j as i64) * self.lattice.get_spin( i as i64, (j + 1) as i64)
            }
        }
        -1.0 * self.interaction_constant * (pairs as f64)
    }

    pub fn calc_magnetization(&self) -> f64
    {
        let mut output: i64 = 0;
        for i in 0..self.lattice.size()
        {
            for j in 0..self.lattice.size()
            {
                output = output + self.lattice.get_spin(i as i64, j as i64);
            }
        }

        (output as f64) / (self.lattice.size().pow(2) as f64)
    }
}