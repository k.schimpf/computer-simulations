mod position;
mod model;
mod widoms_sampling;
mod shing_gubbins;
mod binning;
mod bennett;
mod correlation;

use std::env;
use crate::bennett::bennets;
use crate::correlation::correlation;
use crate::model::Model;
use crate::shing_gubbins::shing_gubbins_sampling;
use crate::widoms_sampling::{deletion, insert, widoms_sampling};

fn widoms(iterations: usize)
{
    let first_model = Model::new(6, 2.0, 432);
    let second_model = Model::new(6, 10.0, 80);

    let result = widoms_sampling(&mut first_model.clone(), iterations, insert);
    println!("First model insert result: {}", result);

    let result = widoms_sampling(&mut second_model.clone(), iterations, insert);
    println!("Second model insert result: {}", result);

    let result = widoms_sampling(&mut first_model.clone(), iterations, deletion);
    println!("First model delete result: {}", result);

    let result = widoms_sampling(&mut second_model.clone(), iterations, deletion);
    println!("Second model delete result: {}", result);
}

fn correlation_time(iterations: usize)
{
    let mut model: Model = Model::new(6, 10.0, 80);

    correlation(&mut model, iterations);
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 3
    {
        return;
    }

    let iterations: usize = args[1].parse().unwrap();


    match args[2].as_str()
    {
        "widoms" => {
            widoms(iterations)
        },
        "shing_gubbins" => {
            shing_gubbins_sampling(6, 2.0, 432, iterations);
            shing_gubbins_sampling(6, 10.0, 80, iterations);
        },
        "bennets" => {
            bennets(6, 2.0, 432, iterations);
            bennets(6, 10.0, 80, iterations);
        },
        "correlation" => {
            correlation_time(iterations);
        }
        _ => {}
    }


}
