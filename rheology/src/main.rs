use std::env;
use std::fs::File;
use crate::binning::Histogram;
use crate::rouse_chain::RouseChain;
use crate::monte_carlo::MonteCarlo;
use crate::langevin::Langevin;
use crate::model::Model;
use std::io::Write;
use crate::vector::Vector3d;


mod vector;
mod rouse_chain;
mod monte_carlo;
mod model;
mod langevin;
mod binning;

fn save_end_to_end(end_to_end: Vec<Vector3d>, name: &str)
{
    let h_name = format!("end_to_end_{}.txt", name);

    let mut output = File::create(h_name.as_str()).unwrap();
    for n in end_to_end.iter()
    {
        writeln!(&mut output, "{}", n).unwrap();
    }
}

fn save_rouse_mode(rouse_mode: Vec<Vector3d>, name: &str)
{
    let h_name = format!("rouse_mode_{}.txt", name);

    let mut output = File::create(h_name.as_str()).unwrap();
    for n in rouse_mode.iter()
    {
        writeln!(&mut output, "{}", n).unwrap();
    }
}

fn iterate(model:  &mut dyn Model, iterations: usize, name: &str)
{
    let mut end_to_end: Vec<Vector3d> = vec![];
    let mut rouse_mode: Vec<Vector3d> = vec![];

    for _ in 0..iterations
    {
        model.step();
        end_to_end.push(model.get_model().end_to_end());
        rouse_mode.push(model.get_model().calc_rouse_mode());
    }

    save_end_to_end(end_to_end, name);
    save_rouse_mode(rouse_mode, name);

}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 2
    {
        return;
    }

    let iterations: usize = args[1].parse().unwrap();

    let mut model = RouseChain::new(64);

    let mut monte_carlo = MonteCarlo::new(model.clone());
    let mut langevin = Langevin::new(model.clone(), 5.0, 0.001);

    iterate(&mut monte_carlo, iterations, "monte_carlo");
    iterate(&mut langevin, iterations, "langevin");
}
