use crate::euler::rev_euler_step;
use crate::myst::myst_step;
use crate::phase_space::PhaseSpace;
use crate::runge_kutta::runge_kutta_step;
use crate::velocity_verlet::velocity_verlet_step;

mod phase_space;
mod euler;
mod runge_kutta;
mod myst;
mod velocity_verlet;

fn main() {
    let mut state = PhaseSpace{position: 1.0, momentum: 1.0};
    state.print();

    for _ in 0..100000
    {
        state = runge_kutta_step(state, 0.001);
        state.print()
    }
}
