use rand_distr::{Normal, Distribution};
use rand::{seq::SliceRandom, Rng};

use std::cell::Cell;
use rand_distr::num_traits::ToPrimitive;

struct Particle
{
    id: u8,
    position: Cell<f64>
}


struct Model1D
{
    move_distr: Normal<f64>,

    line_length: f64,
    particle_size: f64,

    particles: Vec<Particle>
}

impl Model1D
{
    fn generate_particles(line_length: f64, particle_size: f64, particle_number: u8) -> Vec<Particle>
    {
        let mut particles: Vec<Particle> = Vec::new();
        let effective_length = line_length - particle_size;

        if effective_length/particle_number.to_f64().unwrap() < particle_size/2.0
        {
            panic!("To many particles for line length!")
        }

        for i in 0..particle_number
        {
            let position: f64 = particle_size/2.0 + i.to_f64().unwrap() * effective_length/particle_number.to_f64().unwrap();
            let particle = Particle {id: i, position: Cell::new(position)};

            particles.push(particle);
        }

        particles
    }

    fn new(line_length: f64, particle_size: f64, particle_number: u8) -> Model1D
    {
        let normal = Normal::new(0.0, particle_size/2.0).unwrap();
        let particles: Vec<Particle> = Model1D::generate_particles(line_length, particle_size, particle_number);
        
        Model1D {
            move_distr: normal,
            line_length,
            particle_size,
            particles,
        }
    }

    fn check_particle_inside_bounds(&self, particle: &Particle) -> bool
    {
        self.particle_size/2.0 < particle.position.get() && particle.position.get() <= self.line_length - self.particle_size/2.0
    }

    fn check_particle_collides(&self, particle: &Particle) -> bool
    {
        let particle_iterator = self.particles.iter();

        for other_particle in particle_iterator
        {
            if other_particle.id == particle.id
            {
                continue;
            }

            if (other_particle.position.get() - particle.position.get()).abs() < self.particle_size
            {
                return true
            }
        }
        return false
    }

    fn accept_move(&self, particle: &Particle) -> bool
    {
        if !self.check_particle_inside_bounds(particle) // Fail fast
        {
            return false;
        }

        if self.check_particle_collides(particle)
        {
            return false;
        }

        return true;
    }

    fn generate_move_length(&self) -> f64
    {
        self.move_distr.sample(&mut rand::thread_rng())
    }

    fn choose_particle(&self) -> usize
    {
        let len = self.particles.len();
        rand::thread_rng().gen_range(0..len)
    }

    fn step(&self)
    {
        let particle_index = self.choose_particle();
        let move_length = self.generate_move_length();
        let particle = &self.particles[particle_index];
        let old_pos = particle.position.get();

        particle.position.set(old_pos + move_length);

        if !self.accept_move(particle)
        {
            particle.position.set(old_pos);
        }
    }

    fn print(&self)
    {
        let particle_iterator = self.particles.iter();

        for particle in particle_iterator
        {
            print!("{} \t", particle.position.get());
        }
        println!();
    }

    fn calc_wall_number(&self) -> u64
    {
        let mut particle_count = 0;
        let particle_iterator = self.particles.iter();
        for particle in particle_iterator
        {
            if particle.position.get() < self.particle_size * 2.5
            {
                particle_count += 1;
            }
        }
        particle_count
    }

}

fn calc_average_wall_pressure(model: Model1D, iterations: u64) -> f64
{
    let mut particle_count: u64 = 0;
    for _ in 0..iterations{
        model.step();
        particle_count += model.calc_wall_number();
    }

    particle_count.to_f64().unwrap()/iterations.to_f64().unwrap()
}

fn iterate(model: Model1D, iterations: u64)
{
    for _ in 0..iterations{
        model.step();
        model.print()
    }
}

fn main() {

    let iterations: u64 = 100000;
    /*
    for particle_number in 1..18
    {
        let model = Model1D::new(10.0, 0.5, particle_number);
        println!("{}\t{}", particle_number, calc_average_wall_pressure(model, iterations))
    }
    */
    let model = Model1D::new(10.0, 0.5, 10);
    iterate(model, 10000000);
}
