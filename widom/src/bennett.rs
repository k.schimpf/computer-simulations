use crate::model::Model;

fn sample(model: &mut Model, iterations: usize, time_distance: usize, method: fn(&mut Model) -> f64) -> Vec<f64>
{
    let mut energy_change: Vec<f64> = vec![];
    let mut buffer: f64;

    for i in 0..iterations
    {
         model.step_markov();

        if i % time_distance == 0
        {
            buffer = method(model);
            energy_change.push(buffer);
        }
    }

    energy_change
}

fn fermi_dirac(x: f64) -> f64
{
    1.0/(1.0 + x.exp())
}

fn bennets_ratio(first_energy: &Vec<f64>, second_energy: &Vec<f64>, c: f64) -> f64
{
    let first_sum: f64 = first_energy.iter().map(|x| fermi_dirac(x - c)).sum();
    let second_sum: f64 = second_energy.iter().map(|x| fermi_dirac(c - x)).sum();

    first_sum - second_sum
}

fn fermi_dirac_derivative(x: f64) -> f64
{
    x.exp()/(1.0 + x.exp()).powf(2.0)
}

fn bennets_ratio_derivative(first_energy: &Vec<f64>, second_energy: &Vec<f64>, c: f64) -> f64
{
    let first_sum: f64 = first_energy.iter().map(|x| fermi_dirac_derivative(x - c)).sum();
    let second_sum: f64 = second_energy.iter().map(|x| fermi_dirac_derivative(c - x)).sum();

    first_sum + second_sum
}

fn root_finding(first_energy: Vec<f64>, second_energy: Vec<f64>, initial_guess: f64, convergence: f64, max_iterations: usize) -> Option<f64>
{
    let mut c: f64 = initial_guess;
    let mut value: f64;
    let mut derivative: f64;
    let mut iterations: usize = 0;

    loop
    {
        value = bennets_ratio(&first_energy, &second_energy, c);
        derivative = bennets_ratio_derivative(&first_energy, &second_energy, c);

        if derivative == 0.0 || iterations > max_iterations
        {
            return None;
        }else if (value/derivative).abs() < convergence
        {
            return Some(c);
        }

        c = c - value/derivative;
        iterations = iterations + 1;
    }
}

pub fn bennets(length: usize, potential_strength: f64, particle_number: usize, iterations: usize)
{
    let mut first_model = Model::new(length, potential_strength, particle_number);
    let mut second_model = Model::new(length, potential_strength, particle_number+1);

    let first_energy = sample(&mut first_model, iterations, 10, Model::sample_insert);
    let second_energy = sample(&mut second_model, iterations, 10, Model::sample_delete);

    let c = root_finding(first_energy, second_energy, 0.1, 1e-6_f64, 1000000).unwrap();

    let result: f64 = ((particle_number+1) as f64)/(particle_number as f64) * (-c).exp();

    println!("Found c: {}", c);
    println!("Result: {}", result);
}