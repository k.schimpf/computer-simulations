mod position;

#[cfg(test)]
mod tests
{

    use crate::position::Position;

    #[test]
    fn test_position_distance()
    {
        let position = Position{x: 0.0, y: 0.0, z: 0.0};

        assert_eq!(position.distance(&Position { x: 1.0, y: 0.0, z: 0.0}, 6.0), 1.0);
        assert_eq!(position.distance(&Position { x: 3.0, y: 0.0, z: 0.0 }, 6.0), 3.0);
        assert_eq!(position.distance(&Position { x: 4.0, y: 0.0, z: 0.0 }, 6.0), 2.0);

        assert_eq!(position.distance(&Position { x: 0.0, y: 1.0, z: 0.0 }, 6.0), 1.0);
        assert_eq!(position.distance(&Position { x: 0.0, y: 3.0, z: 0.0 }, 6.0), 3.0);
        assert_eq!(position.distance(&Position { x: 0.0, y: 4.0, z: 0.0 }, 6.0), 2.0);

        assert_eq!(position.distance(&Position { x: 0.0, z: 1.0, y: 0.0 }, 6.0), 1.0);
        assert_eq!(position.distance(&Position { x: 0.0, z: 3.0, y: 0.0 }, 6.0), 3.0);
        assert_eq!(position.distance(&Position { x: 0.0, z: 4.0, y: 0.0 }, 6.0), 2.0);
    }
}