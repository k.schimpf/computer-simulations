mod vector;

use std::f64::consts::PI;

use rand_distr::{Normal, Distribution};
use rand_distr::num_traits::ToPrimitive;
use crate::vector::Vector3d;


#[derive(Clone)]
struct Particle
{
    pub position: Vector3d,
    pub momentum: Vector3d
}

struct Model
{
    polymer: Vec<Particle>,
    number_particles: usize,
    time_step: f64
}

impl Model
{
    fn generate_polymer(number_particles: usize) -> Vec<Particle>
    {
        let first_particle: Particle = Particle{ position: Vector3d::get_zero(), momentum: Vector3d::gen_maxwell(1.0)};
        let mut polymer: Vec<Particle> = vec![first_particle];

        let distance_dispersion: f64 = 1.0/(3.0 * (number_particles.to_f64().unwrap() - 1.0));

        for i in 1..number_particles
        {
            let distance = Vector3d::gen_maxwell(distance_dispersion);
            let position = polymer[i-1].position.add(&distance);
            let momentum = Vector3d::gen_maxwell(1.0);
            polymer.push(Particle{position, momentum});
        }

        polymer
    }

    fn new(number_particles: usize, time_step: f64) -> Model
    {
        let polymer = Model::generate_polymer(number_particles);

        Model{polymer, number_particles, time_step}
    }

    fn force_particle(&self, index: usize, particle_position: &Vector3d) -> Vector3d
    {
        let mut potential: Vector3d = Vector3d::get_zero();

        if index > 0
        {
            potential = particle_position.add(
                &self.polymer[index-1].position.multiply(-1.0)
            )
        }

        if index + 1 < self.polymer.len()
        {
            potential = potential.add(
                &particle_position.add(
                    &self.polymer[index+1].position.multiply(-1.0)
                )
            );
        }

        potential.multiply(-3.0 * (self.number_particles.to_f64().unwrap() - 1.0))
    }

    fn step_particle(&self, index: usize) -> Particle
    {
        let particle =  &self.polymer[index];
        let force = self.force_particle(index, &particle.position);

        let mut new_position = particle.position.add(&particle.momentum.multiply(self.time_step));
        new_position = new_position.add(&force.multiply(0.5 * self.time_step.powf(2.0)));

        let new_force = self.force_particle(index, &new_position);

        let new_momentum = particle.momentum.add(&force.add(&new_force).multiply(0.5 * self.time_step));

        Particle{position: new_position, momentum: new_momentum}
    }

    fn step(&mut self)
    {
        let mut next_polymer:Vec<Particle> = vec![];

        for index in 0..self.polymer.len()
        {
            next_polymer.push(self.step_particle(index));
        }

        self.polymer = next_polymer;
    }

    fn redraw_momenta(&mut self)
    {
        for i in 0..self.polymer.len()
        {
            self.polymer[i].momentum = Vector3d::gen_maxwell(1.0);
        }
    }

    fn calc_energy(&self) -> f64
    {
        let velocity_energy: f64 = self.polymer.iter().fold(0.0, |acc, particle| acc + particle.momentum.square());

        let mut position_energy: f64 = 0.0;

        for index in 1..self.polymer.len()
        {
            position_energy = position_energy + self.polymer[index].position.add(&self.polymer[index-1].position.multiply(-1.0)).square()
        }

        position_energy = position_energy * 3.0 * (self.number_particles.to_f64().unwrap() - 1.0);

        velocity_energy + position_energy
    }

    fn calc_center_of_mass(&self) -> Vector3d
    {
        let mut center_of_mass: Vector3d = Vector3d::get_zero();

        for particle in self.polymer.iter()
        {
            center_of_mass = center_of_mass.add(&particle.position);
        }

        center_of_mass.multiply(1.0/self.number_particles.to_f64().unwrap())
    }

    fn calc_rouse_mode(&self) -> Vector3d
    {

        let mut output: Vector3d = Vector3d::get_zero();
        let mut buffer: Vector3d;

        for i in 0..self.polymer.len()
        {
           buffer = self.polymer[i].position.multiply(PI/self.number_particles.to_f64().unwrap() * (i.to_f64().unwrap() - 0.5));
           output = output.add(&buffer);
        }

        output.multiply(1.0/self.number_particles.to_f64().unwrap());

        output
    }

    fn print(&self)
    {
        for particle in self.polymer.iter()
        {
            print!("{} , {},", particle.position, particle.momentum);
        }

        println!();
    }
}

fn iterate(model: &mut Model, steps: usize, apply_thermostat: bool)
{
    println!("{}", model.calc_rouse_mode());
    //model.print();
    for i in 0..steps
    {
        
        if i%4 == 0 && apply_thermostat
        {
            model.redraw_momenta();
        }
        model.step();
        //model.print();
        println!("{}", model.calc_rouse_mode());
    }
}

fn main() {
    let mut model = Model::new(64, 0.00001);

    iterate(&mut model, 50000000, true);
}
