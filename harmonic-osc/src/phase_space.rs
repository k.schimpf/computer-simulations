pub struct PhaseSpace
{
    pub position: f64,
    pub momentum: f64
}

impl PhaseSpace
{
    pub fn print(&self)
    {
        println!("{}\t{}", self.position, self.momentum)
    }
}