use crate::phase_space::PhaseSpace;

pub fn runge_kutta_step(current_state: PhaseSpace, time_step: f64) -> PhaseSpace
{
    let b_1_1: f64 = 1.0 - time_step.powf(2.0)/2.0 + time_step.powf(4.0)/24.0;
    let b_1_2: f64 = time_step - 1.0/6.0 * time_step.powf(3.0);
    let b_2_1: f64 = - b_1_2;
    let b_2_2: f64 = b_1_1;

    let new_position: f64 = current_state.position * b_1_1 + current_state.momentum * b_1_2;
    let new_momentum: f64 = current_state.position * b_2_1 + current_state.momentum * b_2_2;

    PhaseSpace {position: new_position, momentum: new_momentum}
}