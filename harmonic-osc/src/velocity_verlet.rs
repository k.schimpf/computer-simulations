use crate::phase_space::PhaseSpace;

pub fn velocity_verlet_step(current_state: PhaseSpace, time_step: f64) -> PhaseSpace
{
    let new_position: f64 = current_state.position + current_state.momentum * time_step - 1.0/2.0 * current_state.position * time_step.powf(2.0);
    let new_momentum: f64 = current_state.momentum - (current_state.position + new_position)/2.0 * time_step;

    PhaseSpace {position: new_position, momentum: new_momentum}
}