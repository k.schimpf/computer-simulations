use rand::distributions::{Distribution, Uniform};
use crate::rouse_chain::RouseChain;
use crate::model::Model;
use crate::vector::Vector3d;

#[derive(Clone)]
pub struct MonteCarlo
{
    rouse_chain: RouseChain,
    accept_distribution: Uniform<f64>,
    part_distribution: Uniform<usize>
}


impl MonteCarlo
{
    pub fn new(rouse_chain: RouseChain) -> MonteCarlo
    {
        let uniform = Uniform::new_inclusive(0.0, 1.0);
        let particle = Uniform::new(0, rouse_chain.polymer.len());

        MonteCarlo{rouse_chain, accept_distribution: uniform, part_distribution: particle}
    }

    fn move_particle(&mut self, index: usize, displacement: &Vector3d)
    {
        let new_pos = self.rouse_chain.polymer[index].position.add(displacement);
        self.rouse_chain.polymer[index].position = new_pos;
    }

    fn prop_particle(&self) -> usize
    {
        return self.part_distribution.sample(&mut rand::thread_rng())
    }

    fn prop_displacement(&self) -> Vector3d
    {
        Vector3d::gen_uniform(1.0/((self.rouse_chain.polymer.len() as f64) - 1.0).sqrt())
    }

    fn calc_particle_potential(&self, index: usize) -> f64
    {
        let mut potential: f64 = 0.0;

        if index > 0
        {
            potential = potential + self.rouse_chain.calc_particle_potential(index);
        }

        if index + 1 < self.rouse_chain.polymer.len()
        {
            potential = potential + self.rouse_chain.calc_particle_potential(index+1);
        }

        potential
    }

    fn acc_displacement(&self, energy_change: f64) -> bool
    {
        let accept_prop: f64 = ((-energy_change).exp()).min(1.0);

        let sample: f64 = self.accept_distribution.sample(&mut rand::thread_rng());

        return sample <= accept_prop;
    }

    fn step_particle(&mut self, index: usize)
    {
        //let current_potential = self.calc_particle_potential(index);
        let current_potential = self.get_model().calc_energy();
        let prop_displacement = self.prop_displacement();
        self.move_particle(index, &prop_displacement);

        //let next_potential = self.calc_particle_potential(index);
        let next_potential = self.get_model().calc_energy();
        let potential_change = next_potential - current_potential;

        if !self.acc_displacement(potential_change)
        {
            self.move_particle(index, &prop_displacement.multiply(-1.0));
        }
    }
}

impl Model for MonteCarlo
{
    fn step(&mut self)
    {
        for _ in 0..self.get_model().polymer.len()
        {
            let index = self.prop_particle();
            self.step_particle(index);
        }
    }

    fn get_model(&self) -> &RouseChain
    {
        &self.rouse_chain
    }
}
