use std::f64::consts::PI;

use rand_distr::num_traits::ToPrimitive;
use crate::vector::Vector3d;


#[derive(Clone)]
pub struct Particle
{
    pub position: Vector3d,
    pub momentum: Vector3d
}

#[derive(Clone)]
pub struct RouseChain
{
    pub polymer: Vec<Particle>,
}

impl RouseChain
{
    fn generate_polymer(number_particles: usize) -> Vec<Particle>
    {
        let first_particle: Particle = Particle{ position: Vector3d::get_zero(), momentum: Vector3d::gen_maxwell(1.0)};
        let mut polymer: Vec<Particle> = vec![first_particle];

        let distance_dispersion: f64 = 1.0/(3.0 * (number_particles.to_f64().unwrap() - 1.0));

        for i in 1..number_particles
        {
            let distance = Vector3d::gen_maxwell(distance_dispersion);
            let position = polymer[i-1].position.add(&distance);
            let momentum = Vector3d::gen_maxwell(1.0);
            polymer.push(Particle{position, momentum});
        }

        polymer
    }

    pub fn new(number_particles: usize) -> RouseChain
    {
        let polymer = RouseChain::generate_polymer(number_particles);

        RouseChain {polymer}
    }

    fn force_particle(&self, index: usize, particle_position: &Vector3d) -> Vector3d
    {
        let mut potential: Vector3d = Vector3d::get_zero();

        if index > 0
        {
            potential = particle_position.add(
                &self.polymer[index-1].position.multiply(-1.0)
            )
        }

        if index + 1 < self.polymer.len()
        {
            potential = potential.add(
                &particle_position.add(
                    &self.polymer[index+1].position.multiply(-1.0)
                )
            );
        }

        potential.multiply(-3.0 * (self.polymer.len().to_f64().unwrap() - 1.0))
    }

    pub fn calc_particle_potential(&self, index: usize) -> f64
    {
        self.polymer[index].position.add(&self.polymer[index-1].position.multiply(-1.0)).square()
    }

    pub fn calc_energy(&self) -> f64
    {
        let velocity_energy: f64 = self.polymer.iter().fold(0.0, |acc, particle| acc + particle.momentum.square());

        let mut position_energy: f64 = 0.0;

        for index in 1..self.polymer.len()
        {
            position_energy = position_energy + self.calc_particle_potential(index);
        }

        position_energy = position_energy * 3.0 * (self.polymer.len().to_f64().unwrap() - 1.0);

        velocity_energy + position_energy
    }

    pub fn calc_center_of_mass(&self) -> Vector3d
    {
        let mut center_of_mass: Vector3d = Vector3d::get_zero();

        for particle in self.polymer.iter()
        {
            center_of_mass = center_of_mass.add(&particle.position);
        }

        center_of_mass.multiply(1.0/self.polymer.len().to_f64().unwrap())
    }

    pub fn calc_rouse_mode(&self) -> Vector3d
    {

        let mut output: Vector3d = Vector3d::get_zero();
        let mut buffer: Vector3d;

        for i in 0..self.polymer.len()
        {
           buffer = self.polymer[i].position.multiply((PI/self.polymer.len().to_f64().unwrap() * (i.to_f64().unwrap() - 0.5)).cos());
           output = output.add(&buffer);
        }

        output.multiply(1.0/self.polymer.len().to_f64().unwrap());

        output
    }

    pub fn end_to_end(&self) -> Vector3d
    {
        self.polymer[self.polymer.len()-1].position.add(&self.polymer[0].position.multiply(-1.0))
    }

    pub fn structure_factor(&self, q: Vector3d) -> f64
    {
        let scaler = self.polymer.iter().map(|p| p.position.scalar_product(&q));
        let cos_term: f64 = scaler.clone().fold(0.0, |acc, p| acc + p.cos());
        let sin_term: f64 = scaler.fold(0.0, |acc, p| acc + p.sin());

        cos_term.powf(2.0) + sin_term.powf(2.0)
    }

    pub fn print(&self)
    {
        for particle in self.polymer.iter()
        {
            print!("{} , {},", particle.position, particle.momentum);
        }

        println!();
    }
}
