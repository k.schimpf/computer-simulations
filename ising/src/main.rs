mod model;
mod binning;
mod lattice;
mod wang_landau_model;

use std::env;
use crate::binning::Histogram;
use crate::lattice::Lattice;
use crate::model::MetropolisModel;
use crate::wang_landau_model::{wang_landau_magnetization_hist, WangLandauModel};

#[allow(dead_code)]
fn energy_check(model: &mut MetropolisModel, iterations: usize)
{
    let initial_energy = model.calc_energy();
    let mut energy_from_steps = initial_energy;
    let mut energy_difference: f64;

    for _ in 0..iterations
    {
        energy_difference = model.step();
        energy_from_steps = energy_from_steps + energy_difference;
    }

    let end_energy = model.calc_energy();

    println!("Initial energy: {} \t Energy from steps: {} \t End energy: {}", initial_energy, energy_from_steps, end_energy);
}

#[allow(dead_code)]
fn energy_hist(model: &mut MetropolisModel, iterations: usize)
{
    let mut energies: Vec<f64> = vec![model.calc_energy()];
    let mut energy_difference: f64;

    for _ in 0..iterations
    {
        energy_difference = model.step();
        energies.push(energies.last().unwrap() + energy_difference);
    }

    let hist_energies = Histogram::new_from_vec(energies, 100);
    hist_energies.print();
}


#[allow(dead_code)]
fn magn_hist(model: &mut MetropolisModel, iterations: usize) -> Histogram
{
    let mut hist_energies = Histogram::new(400, -1.0, 1.0);

    for _ in 0..iterations
    {
        model.step();
        hist_energies.add_value(&model.calc_magnetization())
    }


    hist_energies
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 2
    {
        return;
    }

    let iterations: usize = args[1].parse().unwrap();

    let J_C: f64 = 0.5 * (1.0 + 2.0_f64.sqrt()).ln();

    let lattice = Lattice::new_rdm(4);

    //let mut model: MetropolisModel = MetropolisModel::new(0.3, lattice.clone());
    //magn_hist(&mut model, iterations).write("hist_j03.tsv").unwrap();

    //let mut model: MetropolisModel = MetropolisModel::new(J_C, lattice.clone());
    //magn_hist(&mut model, iterations).write("hist_jc.tsv").unwrap();

    let mut model: WangLandauModel = WangLandauModel::new(0.5, lattice, 1.2);
    wang_landau_magnetization_hist(&mut model).write("hist_wl_03.tsv").unwrap();
}
