import numpy as np
import matplotlib.pyplot as plt


def main():
    with open("particles-2.tsv") as file:
        input_str = file.read()

    positions = [
        float(val)
        for n, row in enumerate(input_str.split("\n")) for val in row.split("\t") if val != "" and val != " "
        ]

    iteration = [
        n
        for n, row in enumerate(input_str.split("\n")) for val in row.split("\t")  if val != "" and val != " "
        ]

    plt.errorbar(iteration, positions, fmt=".", yerr=0.5)
    plt.grid()
    plt.show()


if __name__ == '__main__':
    main()
