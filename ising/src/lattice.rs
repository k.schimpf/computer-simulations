
#[derive(Clone)]
pub struct Lattice
{
    states: Vec<Vec<bool>>
}

impl Lattice
{
    pub fn new_rdm(n: usize) -> Lattice
    {
        let mut states: Vec<Vec<bool>> = vec![];

        for _ in 0..n
        {
            let mut row: Vec<bool> = vec![];
            for _ in 0..n
            {
                row.push(rand::random())
            }
            states.push(row);
        }
        
        Lattice{states}
    }
    
    pub fn new(n: usize) -> Lattice
    {
        let mut states: Vec<Vec<bool>> = vec![];

        for _ in 0..n
        {
            let mut row: Vec<bool> = vec![];
            for _ in 0..n
            {
                row.push(true)
            }
            states.push(row);
        }
        
        Lattice{states}
    }

    pub fn size(&self) -> usize
    {
        self.states.len()
    }
    
    fn wrap_index(&self, i: i64) -> usize
    {
        (((self.states.len() as i64) + i) as usize) % self.states.len()
    }

    pub fn get_spin(&self, i: i64, j: i64) -> i64
    {
        match self.states[self.wrap_index(i)][self.wrap_index(j)]
        {
            true => 1,
            false => -1
        }
    }

    pub fn flip_spin(&mut self, i: usize, j: usize)
    {
        self.states[i][j] = !self.states[i][j];
    }

    pub fn sum_neighbours(&self, i: i64, j: i64) -> i64
    {
        let mut output: i64 = 0;
        output = output + self.get_spin(i + 1, j);
        output = output + self.get_spin(i - 1, j);
        output = output + self.get_spin(i, j + 1);
        output = output + self.get_spin(i, j - 1);
        output
    }

    #[allow(dead_code)]
    pub fn print(&self)
    {
        for i in 0..self.size()
        {
            for j in 0..self.size()
            {
                print!("{} \t", self.get_spin(i as i64, j as i64))
            }
            print!("\n");
        }
    }
}