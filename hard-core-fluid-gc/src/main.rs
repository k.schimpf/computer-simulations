use rand_distr::num_traits::ToPrimitive;
use crate::box_model1d::BoxModel1D;
use crate::model1d::Model1D;
use crate::model::Model;

mod model;
mod model1d;
mod box_model1d;

fn iterate(mut model: Box<dyn Model>, iterations: u64) -> f64
{
    let mut number: usize = 0;
    model.print();
    for _ in 0..iterations{
        model.step();
        model.print();
        number = number + model.get_number_particles();
    }

    return number.to_f64().unwrap()/iterations.to_f64().unwrap()
}

fn main() {
    //let activity = 8.4;           // approx N = 9
    //let activity = 18.7;          // approx N = 10
    let activity = 55.0;       // approx N = 11

    //let model = Model1D::new(15.0, 1.0, 5, activity);
    let model = BoxModel1D::new(15.0, 1.0, 11, activity);

    let average: f64 = iterate(Box::new(model), 1000000);

    //println!("{}", average);
}
