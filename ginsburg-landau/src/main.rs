use std::cmp::max;

pub struct Model
{
    profile: Vec<f64>,

    z_step: f64,
    length: f64,
    number_of_points: usize,

    epsilon: f64
}

impl Model
{
    fn generate_profile(number_of_points: usize) -> Vec<f64>
    {
        let mut profile: Vec<f64> = std::iter::repeat(-1.0).take(number_of_points * 2).collect();
        profile[2*number_of_points - 1] = 1.0;

        profile
    }

    fn new(length: f64, number_of_points: usize, epsilon: f64) -> Model
    {
        let profile = Model::generate_profile(number_of_points);
        let z_step: f64 = length/(number_of_points as f64);

        Model{profile, z_step, length, number_of_points, epsilon}
    }

    fn calc_chem_potential(&self, index: usize) -> f64
    {
        let slope =  1.0/self.z_step.powf(2.0) * (self.profile[index + 1] + self.profile[index - 1] - 2.0 * self.profile[index]);
        -self.profile[index] + self.profile[index].powf(3.0) - slope
    }

    pub fn step(&mut self) -> Option<f64>
    {
        let mut next = self.profile.clone();
        let mut max_change: f64 = 0.0;
        let mut change: f64;

        for i in 1..self.profile.len() - 1
        {
            change = self.calc_chem_potential(i) * self.epsilon;

            if change.is_nan()
            {
                return None;
            }

            max_change = max_change.max(change.abs());
            next[i] = self.profile[i] - change;
        }

        self.profile = next;

        Some(max_change)
    }

    pub fn print(&self)
    {
        let coords = (0..2*self.number_of_points).map(|x| (x as f64) * self.z_step  - self.length);

        for (x, m) in self.profile.iter().zip(coords)
        {
            println!("{},{}", m, x);
        }
    }
}

pub fn iterate(mut model: Model, threshold: f64, max_iter: usize) -> Model
{
    for _ in 0..max_iter
    {
        if model.step().unwrap() < threshold
        {
            break;
        }
    }

    model
}

pub fn is_stable(mut model: Model, max_iter: usize) -> bool
{
    for _ in 0..max_iter
    {
        if model.step().is_none()
        {
            return false
        }
    }
    return true
}

pub fn stability(length: f64, number_of_points: usize, mut epsilon: f64, max_iter: usize, step: f64) -> f64
{
    loop {
        let model = Model::new(length, number_of_points, epsilon);
        if !is_stable(model, max_iter)
        {
            return epsilon
        }

        epsilon = epsilon + step;
    }
}

fn main() {
    let mut model = Model::new(5.0, 20, 0.03);

    //model = iterate(model, 1e-9, 100000000);
    //model.print();

    println!("N=20: {}", stability(5.0, 20, 0.02, 10000, 1e-5));
    println!("N=40: {}", stability(5.0, 40, 0.002, 10000, 1e-5));
    println!("N=80: {}", stability(5.0, 80, 0.0002, 10000, 1e-5));
}
