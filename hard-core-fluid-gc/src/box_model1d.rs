use std::ptr::null;
use rand::Rng;
use rand::distributions::{Distribution, Uniform};
use rand_distr::num_traits::ToPrimitive;
use crate::model::Model;


struct Particle
{
    position: f64
}

struct Box
{
    particle: Option<Particle>
}

pub struct BoxModel1D
{
    space_distr: Uniform<f64>,
    accept_distr: Uniform<f64>,

    line_length: f64,
    particle_size: f64,
    activity: f64,

    boxes: Vec<Box>
}

impl BoxModel1D
{
    fn generate_boxes(line_length: f64, particle_size: f64) -> Vec<Box>
    {
        let mut boxes: Vec<Box> = Vec::new();
        let number_of_boxes: usize = (line_length/particle_size).floor().to_usize().unwrap() + 1;
        for _ in 0..number_of_boxes
        {
            boxes.push(Box{particle: None});
        }
        boxes
    }

    fn generate_particles(boxes: &mut Vec<Box>, line_length: f64, particle_size: f64, particle_number: u8)
    {

        let effective_length = line_length - particle_size;

        if effective_length/particle_number.to_f64().unwrap() < particle_size/2.0
        {
            panic!("To many particles for line length!")
        }

        for i in 0..particle_number
        {
            let position: f64 = particle_size / 2.0 + i.to_f64().unwrap() * effective_length / particle_number.to_f64().unwrap();
            let box_index = (position / particle_size).floor().to_usize().unwrap();
            let particle = Particle { position };

            if boxes[box_index].particle.is_some()
            {
                panic!("Something went very wrong while generating particles!")
            }

            boxes[box_index].particle = Some(particle);
        }
    }

    pub fn new(line_length: f64, particle_size: f64, particle_number: u8, activity: f64) -> BoxModel1D
    {
        let space_distr: Uniform<f64> = Uniform::new_inclusive(0.0, particle_size);
        let accept_distr: Uniform<f64> = Uniform::new_inclusive(0.0, 1.0);
        let mut boxes = BoxModel1D::generate_boxes(line_length, particle_size);
        BoxModel1D::generate_particles(& mut boxes, line_length, particle_size, particle_number);

        BoxModel1D {
            space_distr,
            accept_distr,
            line_length,
            particle_size,
            boxes,
            activity
        }
    }

    fn check_particle_inside_bounds(&self, particle: &Particle) -> bool
    {
        self.particle_size/2.0 < particle.position && particle.position <= self.line_length - self.particle_size/2.0
    }

    fn check_particles_collides(&self, particle: &Particle, other_particle: &Particle) -> bool
    {
        (other_particle.position - particle.position).abs() < self.particle_size
    }

    fn check_box_particle_collides(&self, box_index: usize, relative_index: i64) -> bool
    {

        let other_box_index: i64 = box_index.to_i64().unwrap() + relative_index;
        let other_box_index = match other_box_index.to_usize()
        {
            Some(val)  => val,
            None => return false
        };

        if  other_box_index >= self.boxes.len()
        {
            return false;
        }

        let particle = match &self.boxes[box_index].particle
        {
            Some(val) => val,
            None => panic!("")
        };

        if let Some(other_particle) = &self.boxes[other_box_index].particle
        {
            self.check_particles_collides(particle, other_particle)
        }else {
            false
        }
    }

    fn check_box_particle_neighbors_collides(&self, box_index: usize) -> bool
    {
        if self.check_box_particle_collides(box_index, 1)
        {
            return true;
        }

        if self.check_box_particle_collides(box_index, -1)
        {
            return true;
        }

        return false;
    }

    fn check_particle_validity(&self, box_index: usize) -> bool
    {
        let particle = match &self.boxes[box_index].particle
        {
            Some(val) => val,
            None => panic!("")
        };

        if !self.check_particle_inside_bounds(particle)
        {
            return false
        }else if self.check_box_particle_neighbors_collides(box_index)
        {
            return false
        }
        return true
    }

    fn accept_insert(&self, box_index: usize) -> bool
    {
        if !self.check_particle_validity(box_index)
        {
            return false
        }

        let accept_prop:f64 = (self.activity).min(1.0);

        let sample = self.accept_distr.sample(&mut rand::thread_rng());

        return sample <= accept_prop;
    }

    fn accept_delete(&self) -> bool
    {
        let accept_prop:f64 = (1.0 / self.activity).min(1.0);

        let sample = self.accept_distr.sample(&mut rand::thread_rng());

        return sample <= accept_prop;
    }

    fn generate_position(&self) -> f64
    {
        self.space_distr.sample(&mut rand::thread_rng())
    }

    fn choose_box(&self) -> usize
    {
        let len = self.boxes.len();
        rand::thread_rng().gen_range(0..len)
    }

    fn step_insert(& mut self, box_index: usize)
    {
        let position = self.generate_position() + box_index.to_f64().unwrap() * self.particle_size;
        let particle = Particle {position };
        self.boxes[box_index].particle = Some(particle);

        if !self.accept_insert(box_index)
        {
            self.boxes[box_index].particle = None;
        }
    }

    fn step_delete(& mut self, box_index: usize)
    {
        if self.accept_delete()
        {
            self.boxes[box_index].particle = None;
        }
    }
}

impl Model for BoxModel1D
{
    fn step(& mut self)
    {
        let box_index = self.choose_box();

        match self.boxes[box_index].particle
        {
            Some(_) => self.step_delete(box_index),
            None => self.step_insert(box_index)
        }
    }

    fn print(&self)
    {
        let box_iterator = self.boxes.iter();

        for particle_box in box_iterator
        {
            match &particle_box.particle
            {
                Some(particle) => print!("{} \t", particle.position),
                None => {}
            }
        }
        println!();
    }

    fn get_number_particles(&self) -> usize
    {
        let mut number: usize = 0;
        let box_iterator = self.boxes.iter();

        for particle_box in box_iterator
        {
            if particle_box.particle.is_some()
            {
                number = number + 1;
            }
        }

        return number;
    }
}