use rand::distributions::{Distribution, Uniform};
use crate::position::Position;

#[derive(Clone)]
pub struct Model
{
    boxes: Vec<Vec<Vec<Vec<Position>>>>,
    particle_number: usize,
    space_distribution: Uniform<f64>,
    accept_distribution: Uniform<f64>,
    particle_distribution: Uniform<usize>,
    potential_strength: f64
}

impl Model
{
    pub fn new(length: usize, potential_strength: f64, particle_number: usize) -> Model
    {
        let boxes: Vec<Vec<Vec<Vec<Position>>>> = std::iter::repeat(
            std::iter::repeat(
                std::iter::repeat(Vec::new()).take(length).collect()
            ).take(length).collect()
        ).take(length).collect();

        let space_distribution: Uniform<f64> = Uniform::new_inclusive(0.0, length as f64);
        let accept_distribution: Uniform<f64> = Uniform::new_inclusive(0.0, 1.0);
        let particle_distribution: Uniform<usize> = Uniform::new_inclusive(0, particle_number-1);

        let mut model = Model{boxes, particle_number, space_distribution, accept_distribution, particle_distribution, potential_strength};
        for _ in 0..particle_number
        {
            let position: Position = Position::from_random(model.space_distribution);
            model.insert(position);
        }

        model
    }

    pub fn insert(&mut self, position: Position)
    {

        let x_index = position.x as usize;
        let y_index = position.y as usize;
        let z_index = position.z as usize;

        self.boxes[x_index][y_index][z_index].push(position);
    }

    fn wrap_index(&self, i: i32) -> usize
    {
        (((self.boxes.len() as i32) + i) as usize) % self.boxes.len()
    }

    fn count_interactions_in_box(&self, position: &Position, x_index: i32, y_index: i32, z_index: i32) -> usize
    {
        let wrap = self.boxes.len() as f64;
        self.boxes[self.wrap_index(x_index)][self.wrap_index(y_index)][self.wrap_index(z_index)].iter().filter(|&pos| position.distance(pos, wrap) < 1.0).count()
    }

    fn count_interactions_neighbours(&self, position: &Position, x_index: i32, y_index: i32, z_index: i32) -> usize
    {
        let dir_x = [-1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1];
        let dir_y = [-1, -1, -1, 0, 0, 0, 1, 1, 1, -1, -1, -1, 0, 0, 1, 1, 1, -1, -1, -1, 0, 0, 0, 1, 1, 1];
        let dir_z = [-1, 0, 1, -1, 0, 1, -1, 0, 1, -1, 0, 1, -1, 1, -1, 0, 1, -1, 0, 1, -1, 0, 1, -1, 0, 1];

        let mut output: usize = 0;

        for i in 0..26
        {
            /*
            if (x_index + dir_x[i]) < 0 || (x_index + dir_x[i]) == (self.boxes.len() as i32)
            {
                continue
            }else if (y_index + dir_y[i]) < 0 || (y_index + dir_y[i]) == (self.boxes.len() as i32)
            {
                continue
            }
            */

            output = output + self.count_interactions_in_box(position, x_index + dir_x[i], y_index + dir_y[i], z_index + dir_z[i])
        }

        output
    }

    pub fn calc_energy_change(&self, position: &Position) -> f64
    {
        let mut output: usize;
        let x_index = position.x as usize;
        let y_index = position.y as usize;
        let z_index = position.z as usize;

        output = self.boxes[x_index][y_index][z_index].len();
        output = output + self.count_interactions_neighbours(&position, x_index as i32, y_index as i32, z_index as i32);

        (output as f64) * self.potential_strength
    }

    fn pop_random_particle(&mut self) -> Option<Position>
    {
        let mut index = self.particle_distribution.sample(&mut rand::thread_rng());

        for row in &mut self.boxes
        {
            for col in row
            {
                for pbox in col
                {
                    if index >= pbox.len()
                    {
                        index = index - pbox.len();
                    }else
                    {
                        return Some(pbox.swap_remove(index));
                    }
                }
            }
        }
        None
    }

    fn accept_move(&self, energy_change: f64) -> bool
    {
        let accept_prop: f64 = ((-energy_change).exp()).min(1.0);

        let sample: f64 = self.accept_distribution.sample(&mut rand::thread_rng());

        return sample <= accept_prop;
    }

    pub fn step_markov(&mut self) -> f64
    {
        let old_position = self.pop_random_particle().unwrap();
        let mut energy_change = -self.calc_energy_change(&old_position);

        let new_position: Position = Position::from_random(self.space_distribution);
        energy_change = energy_change + self.calc_energy_change(&new_position);

        if self.accept_move(energy_change)
        {
            self.insert(new_position);
            return energy_change;
        }else
        {
            self.insert(old_position);
            return 0.0;
        }
    }

    pub fn sample_insert(&mut self) -> f64
    {
        let position: Position = Position::from_random(self.space_distribution);
        self.calc_energy_change(&position)
    }
    
    pub fn sample_delete(&mut self) -> f64
    {
        let position: Position = self.pop_random_particle().unwrap();
        let energy_change = -self.calc_energy_change(&position);
        self.insert(position);

        energy_change
    }
}