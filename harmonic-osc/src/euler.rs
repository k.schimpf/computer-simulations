use crate::phase_space::PhaseSpace;

pub fn euler_step(current_state: PhaseSpace, time_step: f64) -> PhaseSpace
{
    let new_position: f64 = current_state.position + time_step * current_state.momentum;
    let new_momentum: f64 = current_state.momentum - time_step * current_state.position;

    PhaseSpace {position: new_position, momentum: new_momentum}
}

pub fn rev_euler_step(current_state: PhaseSpace, time_step: f64) -> PhaseSpace
{
    let pre_factor = 1.0/(1.0 + time_step.powf(2.0));

    let mut euler_step = euler_step(current_state, time_step);
    euler_step.position = euler_step.position * pre_factor;
    euler_step.momentum = euler_step.momentum * pre_factor;

    euler_step
}