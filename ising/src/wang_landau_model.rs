use rand::distributions::{Distribution, Uniform};
use rand_distr::num_traits::{Pow, ToPrimitive};
use crate::binning::Histogram;
use crate::lattice::Lattice;

pub struct WangLandauModel
{
    lattice: Lattice,
    move_prob: Uniform<usize>,
    acc_prob: Uniform<f64>,
    interaction_constant: f64,
    histogram: Vec<u64>,
    wang_landau_factor: f64
}

impl WangLandauModel
{
    pub fn new(interaction_constant: f64, lattice: Lattice, wang_landau_factor: f64) -> WangLandauModel
    {
        let move_prob: Uniform<usize> = Uniform::new_inclusive(0, lattice.size().pow(2)-1);
        let acc_prob: Uniform<f64> = Uniform::new_inclusive(0.0, 1.0);

        let histogram: Vec<u64> =  std::iter::repeat(0).take(lattice.size().pow(2) * 2 + 1).collect();

        WangLandauModel {lattice, move_prob, acc_prob, interaction_constant, histogram, wang_landau_factor}
    }

    fn choose_particle(&self) -> usize
    {
        self.move_prob.sample(&mut rand::thread_rng())
    }

    fn calculate_energy_difference(&self, i: i64, j: i64) -> f64
    {
        let spin = -self.lattice.get_spin(i, j);
        (-2 * spin * self.lattice.sum_neighbours(i, j)).to_f64().unwrap() * self.interaction_constant
    }

    fn calculate_new_magnetization(&self, i: i64, j: i64, magnetization: i64) -> i64
    {
        let spin = -self.lattice.get_spin(i, j);
        magnetization + 2 * spin
    }

    fn accept_move(&self, energy_difference: f64, new_magnetization: i64) -> bool
    {
        if energy_difference < 0.0
        {
            return true;
        }

        let acceptance_prob = (-1.0 * energy_difference).exp();
        let sample = self.acc_prob.sample(&mut rand::thread_rng());

        let multiplier = self.wang_landau_factor.powf(self.get_histogram(new_magnetization) as f64);
        return sample <= acceptance_prob/multiplier;
    }

    pub fn step(&mut self) -> f64
    {
        let magnetization = self.calc_magnetization();
        self.update_histogram(magnetization);
        self.update_wang_landau_factor();

        let particle_number = self.choose_particle();
        let i = particle_number / self.lattice.size();
        let j = particle_number - i * self.lattice.size();

        let energy_difference = self.calculate_energy_difference(i as i64, j as i64);
        let new_magnetization = self.calculate_new_magnetization(i as i64, j as i64, magnetization);
        match self.accept_move(energy_difference, new_magnetization)
        {
            true => {
                self.lattice.flip_spin(i, j);
                energy_difference
            },
            false => 0.0
        }
    }

    pub fn calc_energy(&self) -> f64
    {
        let mut pairs: i64 = 0;
        for i in 0..self.lattice.size()
        {
            for j in 0..self.lattice.size()
            {
                pairs = pairs + self.lattice.get_spin(i as i64, j as i64) * self.lattice.get_spin((i + 1) as i64, j as i64);
                pairs = pairs + self.lattice.get_spin(i as i64, j as i64) * self.lattice.get_spin( i as i64, (j + 1) as i64)
            }
        }
        -1.0 * self.interaction_constant * (pairs as f64)
    }

    fn get_histogram(&self, magnetization: i64) -> u64
    {
        let shift: i64 = (self.lattice.size().pow(2)) as i64;

        let index = (magnetization + shift) as usize;

        self.histogram[index]
    }

    fn update_histogram(&mut self, magnetization: i64)
    {
        let shift: i64 = (self.lattice.size().pow(2)) as i64;
        //println!("{} {}", magnetization, shift);
        let index = (magnetization + shift) as usize;

        self.histogram[index] = self.histogram[index] + 1;
    }


    fn update_wang_landau_factor(&mut self)
    {
        let max = self.histogram.iter().max().unwrap().to_owned();
        let min = self.histogram.iter().min().unwrap().to_owned();

        if min == 0
        {
            return;
        }


        if (2.0 - (max as f64)/(min as f64)).abs() < 0.1
        {
            println!("Changed f");
            self.wang_landau_factor = self.wang_landau_factor.sqrt();
            self.histogram = std::iter::repeat(0).take(self.lattice.size().pow(2)).collect();
        }
    }


    fn calc_magnetization(&self) -> i64
    {
        let mut output: i64 = 0;
        for i in 0..self.lattice.size()
        {
            for j in 0..self.lattice.size()
            {
                output = output + self.lattice.get_spin(i as i64, j as i64);
            }
        }
        output
    }


    pub fn calc_magnetization_per_spin(&self) -> f64
    {
        let magnetization = self.calc_magnetization();

        (magnetization as f64) / (self.lattice.size().pow(2) as f64)
    }


    pub fn is_ready(&self) -> bool
    {
        (1.0 - self.wang_landau_factor).abs() < 1e-6
    }
}

#[allow(dead_code)]
pub fn wang_landau_magnetization_hist(model: &mut WangLandauModel) -> Histogram
{
    let mut hist_energies = Histogram::new(100, -1.0, 1.0);

    while!model.is_ready()
    {
        //println!("{}", model.wang_landau_factor);
        model.step();
        hist_energies.add_value(&model.calc_magnetization_per_spin())
    }

    hist_energies
}