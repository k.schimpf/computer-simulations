use std::collections::VecDeque;
use std::thread;
use std::thread::JoinHandle;
use crate::binning::Histogram;
use crate::model::Model;

fn sample(model: &mut Model, iterations: usize, method: fn(&mut Model) -> f64) -> Vec<f64>
{
    let mut energy_change: Vec<f64> = vec![];
    let mut handles: VecDeque<JoinHandle<f64>> = VecDeque::new();

    for _ in 0..iterations
    {
        let mut model_snapshot = model.clone();
        handles.push_back(
            thread::spawn(move || {
                method(&mut model_snapshot)
            })
        );
        model.step_markov();

        if handles.len() > 12
        {
            energy_change.push(handles.pop_front().unwrap().join().unwrap());
        }
    }

    for handler in handles
    {
        energy_change.push(handler.join().unwrap());
    }

    energy_change
}

pub fn shing_gubbins_sampling(length: usize, potential_strength: f64, particle_number: usize, iterations: usize)
{
    let mut insert_model = Model::new(length, potential_strength, particle_number);
    let mut delete_model = Model::new(length, potential_strength, particle_number+1);

    let energy_change_insert = sample(&mut insert_model, iterations, Model::sample_insert);
    let mut energy_change_delete = sample(&mut delete_model, iterations, Model::sample_delete);
    energy_change_delete = energy_change_delete.iter().map(|x| -1.0 * x).collect();

    let hist = Histogram::new(35, -10.0, 60.0);

    let mut hist_insert = hist.clone();
    hist_insert.insert_all(&energy_change_insert);

    let mut hist_delete = hist.clone();
    hist_delete.insert_all(&energy_change_delete);

    let insert_path = format!("p_insert_{}.txt", particle_number);
    let delete_path = format!("p_delete_{}.txt", particle_number);

    hist_insert.write(insert_path.as_str()).unwrap();
    hist_delete.write(delete_path.as_str()).unwrap();
}