use std::fs::File;
use std::io::Write;

pub struct Histogram
{
    bins: Vec<u64>,

    bin_labels: Vec<f64>,
    min_value: f64,
    max_value: f64,
    l: f64
}

impl Histogram
{
    pub fn add_value(&mut self, value: &f64)
    {
        let index = ((value-self.min_value)/self.l) as usize;
        self.bins[index] = self.bins[index] + 1;
    }

    pub fn new(n: usize, min_value: f64, max_value: f64) -> Histogram
    {
        let bins: Vec<u64> = std::iter::repeat(0).take(n+1).collect();

        let l = (max_value - min_value)/(n as f64);

        let bin_labels: Vec<f64> = (0..n+1).map(|x| min_value + ((x as f64) + 0.5) * l).collect();

        Histogram {bins, bin_labels, min_value, max_value, l}
    }

    pub fn new_from_vec(input: Vec<f64>, n: usize) -> Histogram
    {
        let min_value = input.iter().min_by(|a, b| a.partial_cmp(b).unwrap()).unwrap();
        let max_value = input.iter().max_by(|a, b| a.partial_cmp(b).unwrap()).unwrap();

        let mut histogram = Histogram::new(n, min_value.clone(), max_value.clone());

        for value in input.iter()
        {
            histogram.add_value(value);
        }

        histogram
    }

    pub fn print(&self)
    {
        for n in 0..self.bins.len()
        {
            println!("{}\t{}", self.bin_labels[n], self.bins[n]);
        }
    }

    pub fn write(&self, path: &str) -> std::io::Result<()>
    {
        let mut output = File::create(path)?;
        for n in 0..self.bins.len()
        {
            writeln!(&mut output, "{}\t{}", self.bin_labels[n], self.bins[n])?;
        }

        Ok(())
    }
}