use std::collections::VecDeque;
use std::thread;
use std::thread::JoinHandle;
use crate::model::Model;


fn insert(mut model: Model, iterations: usize) -> Vec<f64>
{
    let mut energy_change: Vec<f64> = vec![];
    for _ in 0..iterations
    {
        energy_change.push(model.sample_insert());
    }
    energy_change
}

fn average(input: &Vec<f64>) -> f64
{
    let sum: f64 = input.iter().sum();
    sum/(input.len() as f64)
}

fn multi_average(first: &Vec<f64>, second: &Vec<f64>) -> f64
{
    let sum: f64 = first.iter().zip(second.iter()).map(|(x, y)| x * y).sum();
    sum/(first.len() as f64)
}

fn calc_correlation(first: &Vec<f64>, second: &Vec<f64>) -> f64
{
    let first_average = average(first);
    let second_average = average(second);

    let first_variance = first.iter().map(|x| (x - first_average).powf(2.0)).sum::<f64>().sqrt();
    let second_variance = second.iter().map(|x| (x - second_average).powf(2.0)).sum::<f64>().sqrt();

    let sum: f64 = first.iter().zip(second.iter()).map(|(x, y)| (x - first_average) * (y - second_average)).sum();

    sum/(first_variance + second_variance)
}

pub fn correlation(model: &mut Model, iterations: usize)
{
    let mut energy_change: Vec<Vec<f64>> = vec![];

    let mut handles: VecDeque<JoinHandle<Vec<f64>>> = VecDeque::new();

    for _ in 0..iterations
    {
        let model_snapshot = model.clone();
        handles.push_back(
            thread::spawn(move || {
                insert(model_snapshot, 10000)
            })
        );
        model.step_markov();

        if handles.len() > 12
        {
            energy_change.push(handles.pop_front().unwrap().join().unwrap());
        }
    }

    for handler in handles
    {
        energy_change.push(handler.join().unwrap());
    }

    let first_average = average(&energy_change[0]);
    let mut correlation: f64;

    for (t, iteration) in energy_change[1..].iter().enumerate()
    {
        correlation = calc_correlation(&iteration, &energy_change[0]);
        println!("Correlation for t = {}: {}", t, correlation);
    }
}