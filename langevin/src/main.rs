use std::cell::Cell;
use crate::phase_space::PhaseSpace;
use rand_distr::{Normal, Distribution};
use rand_distr::num_traits::{Pow, ToPrimitive};
use std::thread;
use std::thread::JoinHandle;

mod phase_space;

struct Model
{
    state: PhaseSpace,
    friction_coefficient: f64,
    time_step: f64,
    gaussian: Normal<f64>
}

impl Model
{
    pub fn new(initial_state: PhaseSpace, friction_coefficient: f64, time_step: f64) -> Model
    {
        Model {
            state: initial_state,
            friction_coefficient,
            time_step,
            gaussian: Normal::new(0.0, 1.0).unwrap()
        }
    }

    fn calc_noise(&self) -> f64
    {
        let weight = (2.0 * self.friction_coefficient * self.time_step).sqrt();
        let random = self.gaussian.sample(&mut rand::thread_rng());

        weight * random
    }

    fn calc_scaling(&self) -> f64
    {
        1.0/(1.0 + 0.5 * (self.friction_coefficient * self.time_step))
    }

    pub fn langevin_step(&self)
    {
        let noise:f64 = self.calc_noise();
        let scaling:f64 = self.calc_scaling();

        let new_pos:f64 = self.state.get_pos()
            + self.state.get_mom() * scaling * self.time_step
            - self.state.get_pos() * scaling * self.time_step.pow(2) * 0.5
            + noise * scaling * self.time_step * 0.5;

        let new_mom:f64 = self.state.get_mom()
            + 0.5 * self.time_step * (-new_pos - self.state.get_pos())
            + scaling * noise;

        self.state.set_pos(new_pos);
        self.state.set_mom(new_mom);
    }

    pub fn print(&self)
    {
        self.state.print()
    }

    pub fn get_state(&self) -> &PhaseSpace
    {
        &self.state
    }
}

fn iterate(model: &Model, iterations: usize)
{
    model.print();
    for _ in 0..iterations
    {
        model.langevin_step();
        model.print();
    }
}

fn calc_average(time_step: f64, iterations: usize) -> PhaseSpace
{

    let initial_state = PhaseSpace::new(1.0, 1.0);
    let average = initial_state.clone();

    let model = Model::new(initial_state, 0.1, time_step);
    let mut current_state: &PhaseSpace;

    for _ in 0..iterations
    {
        model.langevin_step();
        current_state = model.get_state();
        average.set_pos(average.get_pos() + current_state.get_pos().pow(2));
        average.set_mom(average.get_mom() + current_state.get_mom().pow(2));
    }

    average.set_pos(average.get_pos()/iterations.to_f64().unwrap());
    average.set_mom(average.get_mom()/iterations.to_f64().unwrap());

    average
}

fn sample_average(iterations: usize, precision: usize)
{
    let mut handles: Vec<JoinHandle<PhaseSpace>> = vec![];
    for i in 0..precision
    {
        let handle = thread::spawn(move || {
            calc_average(10.0.pow(-i.to_f64().unwrap()), iterations)
        });
        handles.push(handle);
    }

    for _ in 0..precision
    {
        let handle = handles.remove(0);
        handle.join().unwrap().print();
    }
}

fn main() {
    //let initial_state = PhaseSpace::new(1.0, 1.0);
    //let model = Model::new(initial_state, 0.1, 0.01);

    //iterate(&model, 10000000);
    //calc_average(1e-5, 1000000000).print();
    sample_average(100000000, 20);
}
