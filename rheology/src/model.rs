use crate::rouse_chain::RouseChain;

pub trait Model
{
    fn step(&mut self);
    fn get_model(&self) -> &RouseChain;
}