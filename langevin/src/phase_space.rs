use std::cell::Cell;

#[derive(Clone)]
pub struct PhaseSpace
{
    position: Cell<f64>,
    momentum: Cell<f64>
}

impl PhaseSpace
{
    pub fn new(position: f64, momentum: f64) -> PhaseSpace
    {
        PhaseSpace {position: Cell::new(position), momentum: Cell::new(momentum)}
    }

    pub fn print(&self)
    {
        println!("{}\t{}", self.position.get(), self.momentum.get())
    }

    pub fn get_pos(&self) -> f64
    {
        self.position.get()
    }

    pub fn get_mom(&self) -> f64
    {
        self.momentum.get()
    }

    pub fn set_pos(&self, new_position: f64)
    {
        self.position.set(new_position)
    }

    pub fn set_mom(&self, new_momentum: f64)
    {
        self.momentum.set(new_momentum)
    }
}