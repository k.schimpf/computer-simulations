import numpy as np
import matplotlib.pyplot as plt


def main():
    with open("particles-9-mod1.tsv") as file:
        input_str = file.read()

    number_particles = np.array(
        [len([x for x in row.split("\t") if x != "" and x != " "]) for row in input_str.split("\n")])

    number_of_steps = len(number_particles)
    numbers = np.arange(0, 20)
    number_prob = np.array([len(number_particles[number_particles == x]) for x in numbers])
    number_prob = number_prob/number_of_steps

    plt.plot(numbers[number_prob != 0], np.log(number_prob[number_prob != 0]), ".--")
    plt.xlim(0, 15)
    plt.xlabel("M")
    plt.ylabel("log P(M)")
    plt.show()

    print(np.average(number_particles))


if __name__ == '__main__':
    main()
