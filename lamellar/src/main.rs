use std::cmp::max;
use std::f64::consts::PI;

pub struct Model
{
    profile: Vec<f64>,

    z_step: f64,
    length: f64,
    number_of_points: usize,

    epsilon: f64,
    alpha: f64
}

impl Model
{
    fn generate_profile(number_of_points: usize) -> Vec<f64>
    {
        let mut profile: Vec<f64> = std::iter::repeat(0.005).take(number_of_points * 2).collect();
        profile[0] = 0.0;
        profile[1] = 0.0;
        profile[2*number_of_points - 2] = 0.0;
        profile[2*number_of_points - 1] = 0.0;

        profile
    }

    fn new(length: f64, number_of_points: usize, epsilon: f64, alpha: f64) -> Model
    {
        let profile = Model::generate_profile(number_of_points);
        let z_step: f64 = length/(number_of_points as f64);

        Model{profile, z_step, length, number_of_points, epsilon, alpha}
    }

    fn calc_slope_2(&self, index: usize, power: f64) -> f64
    {
        1.0/self.z_step.powf(2.0) * (self.profile[index + 1].powf(power) + self.profile[index - 1].powf(power) - 2.0 * self.profile[index].powf(power))
    }

    fn calc_slope_4(&self, index: usize) -> f64
    {
        let slope = -self.profile[index + 2] + 4.0 * self.profile[index + 1] - 6.0 * self.profile[index + 1] + 4.0 * self.profile[index - 1] - self.profile[index - 2];
        slope/self.z_step.powf(4.0)
    }

    fn calc_chem_potential(&self, index: usize) -> f64
    {
        let change = (self.calc_slope_2(index, 2.0) + self.calc_slope_2(index, 3.0) * self.profile[index].powf(2.0) - self.calc_slope_2(index, 1.0)) - self.calc_slope_4(index);

        change - self.alpha * self.profile[index]
    }

    pub fn step(&mut self) -> Option<f64>
    {
        let mut next = self.profile.clone();
        let mut max_change: f64 = 0.0;
        let mut change: f64;

        for i in 2..self.profile.len() - 2
        {
            change = self.calc_chem_potential(i) * self.epsilon;

            if change.is_nan()
            {
                return None;
            }

            max_change = max_change.max(change.abs());
            next[i] = self.profile[i] + change;
        }

        self.profile = next;

        Some(max_change)
    }

    pub fn print(&self)
    {
        let coords = (0..2*self.number_of_points).map(|x| (x as f64) * self.z_step  - self.length);

        for (x, m) in self.profile.iter().zip(coords)
        {
            println!("{},{}", m, x);
        }
    }
}

pub fn iterate(mut model: Model, threshold: f64, max_iter: usize) -> Model
{
    for _ in 0..max_iter
    {
        if model.step().unwrap() < threshold
        {
            break;
        }
    }

    model
}


fn main() {
    let alpha: f64 = 0.24;
    let length: f64 = 2.0 * PI/alpha.powf(0.25);

    let mut model = Model::new(length, 32, 0.0003, alpha);

    model = iterate(model, 1e-8, 100000000);
    model.print();
}
