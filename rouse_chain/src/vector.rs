use core::fmt;

use rand_distr::{Normal, Distribution};

#[derive(Clone)]
pub struct Vector3d
{
    pub x: f64,
    pub y: f64,
    pub z: f64
}

impl Vector3d
{
    pub fn get_zero() -> Vector3d
    {
        Vector3d {x: 0.0, y: 0.0, z: 0.0}
    }

    pub fn gen_maxwell(dispersion: f64) -> Vector3d
    {
        let normal = Normal::new(0.0, dispersion).unwrap();

        Vector3d {
            x: normal.sample(&mut rand::thread_rng()),
            y: normal.sample(&mut rand::thread_rng()),
            z: normal.sample(&mut rand::thread_rng())
        }
    }

    pub fn add(&self, other: &Vector3d) -> Vector3d
    {
        Vector3d {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z
        }
    }

    pub fn multiply(&self, multiplier: f64) -> Vector3d
    {
         Vector3d {
            x: self.x * multiplier,
            y: self.y * multiplier,
            z: self.z * multiplier
        }
    }

    pub fn square(&self) -> f64
    {
        self.x.powf(2.0) + self.y.powf(2.0) + self.z.powf(2.0)
    }
}

impl fmt::Display for Vector3d
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
    {
        write!(f, "{}, {}, {}", self.x, self.y, self.z)
    }
}