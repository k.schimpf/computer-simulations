use std::collections::VecDeque;
use std::thread;
use std::thread::JoinHandle;
use crate::model::Model;

pub fn insert(mut model: Model, iterations: usize) -> f64
{
    let mut energy_change_step: f64 = 0.0;
    for _ in 0..iterations
    {
        let boltzmann_weight = (-1.0 * model.sample_insert()).exp();
        energy_change_step = energy_change_step + boltzmann_weight;
    }
    energy_change_step/(iterations as f64)
}

pub fn deletion(mut model: Model, iterations: usize) -> f64
{
    let boltzmann_weight = (model.sample_delete()).exp();

    boltzmann_weight
}

pub fn widoms_sampling(model: &mut Model, iterations: usize, method: fn(Model, usize) -> f64) -> f64
{
    let mut energy_change: f64 = 0.0;

    let mut handles: VecDeque<JoinHandle<f64>> = VecDeque::new();

    for _ in 0..iterations
    {
        let model_snapshot = model.clone();
        handles.push_back(
            thread::spawn(move || {
                method(model_snapshot, iterations)
            })
        );
        model.step_markov();

        if handles.len() > 12
        {
            energy_change = energy_change + handles.pop_front().unwrap().join().unwrap();
        }
    }

    for handler in handles
    {
        energy_change = energy_change + handler.join().unwrap();
    }

    energy_change/(iterations as f64)
}